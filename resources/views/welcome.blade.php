<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="crypto, mining, animation, example, examples">
    <meta name="author" content="Creativegigs">
    <meta name="description" content="cryto is a beautiful website template designed for bitcoin crypto currency mining and exchange websites.">
    <meta name='og:image' content='images/home/ogg.png'>
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>currencyRateApp by. FestikCZ</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="images/fav-icon/icon.png">

    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- Color Css -->
    <link rel="stylesheet" type="text/css" href="css/color-one.css">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="vendor/html5shiv.js"></script>
    <script src="vendor/respond.js"></script>
    <![endif]-->

</head>

<body>
<div class="main-page-wrapper">


    <!-- ********************** Loading Transition ************************ -->
    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>

    <div class="html-top-content">
        <!-- ********************** Theme Top Banne & Header ************************ -->
        <div class="theme-top-section">
            <!-- ^^^^^^^^^^^^^^^^^ Theme Menu ^^^^^^^^^^^^^^^ -->


            <!-- ^^^^^^^^^^^^^^^^^ Theme Banner ^^^^^^^^^^^^^^^ -->
            <div id="theme-banner" class="theme-banner-one">
                <div class="round-shape-one"></div>
                <div class="round-shape-two"><img src="images/icon/4.png" alt=""></div>
                <div class="round-shape-three"></div>
                <div class="container">
                    <form action="/" method="POST">
                        {{ csrf_field() }}
                    <div class="main-text-wrapper">
                        <h1>Exchange rate calculation</h1>
                        <div class="row" style="padding-top: 100px; padding-bottom: 30px;">
                            <div class="col-md-6">
                                <label>Value*</label>
                                <input style="width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;" type="text" placeholder="80" name="value">
                            </div>
                        </div>
                        <div class="row" style="padding-top: 30px; padding-bottom: 100px;">
                            <div class="col-md-6">
                                <label>From*</label>
                                <select style="width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;" name="rateFrom">
                                    @foreach ($rates as $rate)
                                        <option value="{{ $rate->name }}">{{ $rate->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>To*</label>
                                <select style="width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;" name="rateTo">
                                    @foreach ($rates as $rate)
                                        <option value="{{ $rate->name }}">{{ $rate->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-6">
                                <button type="submit" style="width: 140px;
    line-height: 55px;
    color: #fff;
    background: #fec25e;
    font-size: 18px;
    border-radius: 0px;
    text-align: center;" value="Calculate"> Calculate </button>

                            </div>
                            <div class="col-md-6">
                                <h2>{{ $currencyTransform }} {{ $tranformTo }}</h2>
                            </div>
                        </div>

                    </div>
                    </form>
                </div>
                <div class="container">
                    <div class="stock-market-price">
                        <div id="market-rate">
                            <div class="item">
                                <div class="main-wrapper">
                                    <div class="amount">{{ $topDestination }}</div>
                                    <h6 class="title">Popular destination</h6>
                                    <div class="current-info range-down">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="main-wrapper">
                                    <div class="amount">{{ $totalRequests }}</div>
                                    <h6 class="title">Total requests</h6>
                                    <div class="current-info range-up">

                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="main-wrapper">
                                    <div class="amount">{{ $totalUsd }} $</div>
                                    <h6 class="title">Total USD</h6>
                                    <div class="current-info range-up">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="main-wrapper">
                                    <div class="amount">{{ $lastTotalUsd }} $</div>
                                    <h6 class="title">Last total USD</h6>
                                    <div class="current-info range-down">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- /.stock-market-price -->
                </div>
            </div> <!-- /.theme-banner-one -->
        </div> <!-- /.theme-top-section -->






        <!--
        =====================================================
            Partner Slider
        =====================================================
        -->
        <div class="partner-section">
            <div class="container">
                <div class="partner-slider">
                    <div class="item"><a href="#"><img src="https://www.startupjobs.cz/uploads/174QF1KISW8Fpurple-technology-logo-214834516224.png" alt=""></a></div>
                    <div class="item"><a href="#"><img src="https://www.startupjobs.cz/uploads/174QF1KISW8Fpurple-technology-logo-214834516224.png" alt=""></a></div>
                    <div class="item"><a href="#"><img src="https://www.startupjobs.cz/uploads/174QF1KISW8Fpurple-technology-logo-214834516224.png" alt=""></a></div>
                    <div class="item"><a href="#"><img src="https://www.startupjobs.cz/uploads/174QF1KISW8Fpurple-technology-logo-214834516224.png" alt=""></a></div>
                    <div class="item"><a href="#"><img src="https://www.startupjobs.cz/uploads/174QF1KISW8Fpurple-technology-logo-214834516224.png" alt=""></a></div>
                </div>
            </div>
        </div>




        <!--
        =====================================================
            Footer
        =====================================================
        -->
        <footer class="theme-footer">
            <div class="container">
                <div class="inner-wrapper">
                    <div class="top-footer-data-wrapper">
                        <div class="row">
                            <div class="col-lg-4 col-sm-6 footer-logo">
                                <div class="logo"><h1>AntlLukas</h1></div>
                                <a href="#" class="email">info@festik.cz</a>
                                <a href="#" class="mobile">+420 605 051 586</a>
                            </div> <!-- /.footer-logo -->

                        </div> <!-- /.row -->
                    </div> <!-- /.top-footer-data-wrapper -->

                </div>
            </div> <!-- /.container -->
        </footer>

    </div> <!-- /.html-top-content -->



    <!-- Scroll Top Button -->
    <button class="scroll-top tran3s color-one-bg">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
    </button>




    <!-- Js File_________________________________ -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- jQuery -->
    <script src="vendor/jquery.2.2.3.min.js"></script>
    <!-- Popper js -->
    <script src="vendor/popper.js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Vendor js _________ -->
    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Language Stitcher -->
    <script src="vendor/language-switcher/jquery.polyglot.language.switcher.js"></script>
    <!-- js count to -->
    <script src="vendor/jquery.appear.js"></script>
    <script src="vendor/jquery.countTo.js"></script>
    <!-- Fancybox -->
    <script src="vendor/fancybox/dist/jquery.fancybox.min.js"></script>
    <!-- owl Carousel -->
    <script src="vendor/owl-carousel/owl.carousel.min.js"></script>
    <!-- AOS js -->
    <script src="vendor/aos-next/dist/aos.js"></script>



    <!-- Theme js -->
    <script src="js/theme.js"></script>

</div> <!-- /.main-page-wrapper -->
</body>
</html>
