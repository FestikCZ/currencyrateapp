<?php

namespace App\Http\Hydrator;

use App\Http\Entity\CurrencyRateEntity;
use PHPUnit\Framework\TestCase;

class CurrencyRateHydratorTest extends TestCase
{

    /**
     * @test
     */
    public function hydrate_happyPath_array()
    {
        $hydrator = new CurrencyRateHydrator();
        $hydrate = $hydrator->hydrate('test1', 50);

        $this->assertEquals(new CurrencyRateEntity('test1', 50), $hydrate);

    }
}
