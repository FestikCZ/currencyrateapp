<?php

namespace App\Http\Entity;

use PHPUnit\Framework\TestCase;

class CurrencyRateEntityTest extends TestCase
{
    /**
     * @test
     */
    public function getValue_happyPatch_true()
    {
        $currencyRateEntity = new CurrencyRateEntity('test1', 60);
        $this->assertEquals(60, $currencyRateEntity->getValue());
    }

    /**
     * @test
     */
    public function setValue_happyPatch_true()
    {
        $currencyRateEntity = new CurrencyRateEntity('test1', 60);
        $currencyRateEntity->setValue(80);
        $this->assertEquals(80, $currencyRateEntity->getValue());
    }

    /**
     * @test
     */
    public function getName_happyPatch_true()
    {
        $currencyRateEntity = new CurrencyRateEntity('test1', 60);
        $this->assertEquals('test1', $currencyRateEntity->getName());
    }

    /**
     * @test
     */
    public function setName_happyPatch_true()
    {
        $currencyRateEntity = new CurrencyRateEntity('test1', 60);
        $currencyRateEntity->setName('test2');
        $this->assertEquals('test2', $currencyRateEntity->getName());
    }
}
