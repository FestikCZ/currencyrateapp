<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CurrencyRateLog extends Model
{

    protected $table = 'currency_rate_logs';
    protected $primaryKey = 'id';

    public function saveCurrencyRateLog($from, $to, $value, $rate, $usd){

        $this->from = $from;
        $this->to = $to;
        $this->value = $value;
        $this->rate = ($value / $rate);
        $this->usdTotal = $usd;

        $this->save();
    }

    public function getPopularDestination(){
        return DB::table($this->table)->select('to', DB::raw('COUNT("to") as count'))->groupBy('to')->orderBy('count', 'DESC')->get()->first();
    }

    public function getTotalRequests(){
        return DB::table($this->table)->select(DB::raw('COUNT("to") as count'))->get()->first();
    }

    public function getTotalUsd(){
        return DB::table($this->table)->sum('usdTotal');
    }

    public function getLastTotalUsd(){
        return DB::table($this->table)->select('usdTotal')->orderBy('id', 'DESC')->limit(1)->get()->first();
    }




}
