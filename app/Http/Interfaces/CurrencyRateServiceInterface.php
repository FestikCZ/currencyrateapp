<?php

namespace App\Http\Interfaces;

interface CurrencyRateServiceInterface
{
    public function calculateCurrency(string $from, string $to, float $value): float;

    public function getAllRates(): array;
}
