<?php

namespace App\Http\Controllers;

use App\CurrencyRateLog;
use App\Http\Interfaces\CurrencyRateServiceInterface;
use App\Http\Services\CurrencyRateService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $currencyRateService;

    public function __construct(CurrencyRateService $currencyRateService)
    {
        $this->currencyRateService = $currencyRateService;
    }

    public function test()
    {
        return view('welcome', [
            'rates' => $this->currencyRateService->getAllRates(),
            'currencyTransform' => '',
            'tranformTo' => '',
            'topDestination' => (isset($this->currencyRateService->getPopularDestination()->to) ? $this->currencyRateService->getPopularDestination()->to : 'NoData'),
            'totalRequests' => (isset($this->currencyRateService->getTotalRequests()->count) ? $this->currencyRateService->getTotalRequests()->count : 'NoData'),
            'totalUsd' => $this->currencyRateService->getTotalUsd(),
            'lastTotalUsd' => (isset($this->currencyRateService->getLastTotalUsd()->usdTotal) ? $this->currencyRateService->getLastTotalUsd()->usdTotal : 'NoData')
        ]);
    }

    public function calculate(Request $request)
    {
        $request->validate([
            'value' => 'required|integer',
            'rateFrom' => 'required',
            'rateTo' => 'required',
        ]);

        return view('welcome',
            [
                'rates' => $this->currencyRateService->getAllRates(),
                'currencyTransform' => $this->currencyRateService->calculateCurrency($request->input('rateFrom'),
                    $request->input('rateTo'), $request->input('value')),
                'tranformTo' => $request->input('rateTo'),
                'topDestination' => (isset($this->currencyRateService->getPopularDestination()->to) ? $this->currencyRateService->getPopularDestination()->to : 'NoData'),
                'totalRequests' => (isset($this->currencyRateService->getTotalRequests()->count) ? $this->currencyRateService->getTotalRequests()->count : 'NoData'),
                'totalUsd' => $this->currencyRateService->getTotalUsd(),
                'lastTotalUsd' => (isset($this->currencyRateService->getLastTotalUsd()->usdTotal) ? $this->currencyRateService->getLastTotalUsd()->usdTotal : 'NoData')

            ]);
    }

}
