<?php

namespace App\Http\Services;

use AmrShawky\Currency\Currency;
use App\CurrencyRateLog;
use App\Http\Hydrator\CurrencyRateHydrator;
use App\Http\Interfaces\CurrencyRateServiceInterface;

class CurrencyRateService implements CurrencyRateServiceInterface
{
    private $currency;
    private $currencyRateHydrator;
    private $currencyRateLog;

    public function __construct(Currency $currency, CurrencyRateHydrator $currencyRateHydrator, CurrencyRateLog $currencyRateLog)
    {
        $this->currency = $currency;
        $this->currencyRateHydrator = $currencyRateHydrator;
        $this->currencyRateLog = $currencyRateLog;
    }

    public function calculateCurrency(string $from, string $to, float $value): float
    {
        $rate = $this->currency->convert()->from($from)->to($to)->amount($value)->get();
        $usd = $this->currency->convert()->from($from)->to('USD')->amount($value)->get();

        $this->currencyRateLog->saveCurrencyRateLog($from, $to, $value, $rate, $usd);

        return $this->currency->convert()->from($from)->to($to)->amount($value)->get();
    }

    public function getAllRates(): array
    {
        $allRates = $this->currency->rates()->latest()->get();
        foreach ($allRates as $key => $value) {
            $rates[] = $this->currencyRateHydrator->hydrate($key, $value);
        }

        return $rates;
    }

    public function getPopularDestination(){
        return $this->currencyRateLog->getPopularDestination();
    }

    public function getTotalRequests(){
        return $this->currencyRateLog->getTotalRequests();
    }

    public function getTotalUsd(){
        return $this->currencyRateLog->getTotalUsd();
    }

    public function getLastTotalUsd(){
        return $this->currencyRateLog->getLastTotalUsd();
    }

}
