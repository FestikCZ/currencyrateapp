<?php

namespace App\Http\Hydrator;

use App\Http\Entity\CurrencyRateEntity;

class CurrencyRateHydrator
{
    public function hydrate($name, $value)
    {
        return new CurrencyRateEntity(
            (string) $name,
            (float) $value
        );
    }
}
